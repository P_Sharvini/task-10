#!/usr/bin/env python
# coding: utf-8

# In[41]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split


# In[42]:


csvdata =pd.read_csv('Temperature.csv')
csvdata


# In[43]:


X = csvdata.iloc[:,0:1].values
y = csvdata.iloc[:,1].values

x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)


# In[44]:


x_train, x_test, y_train, y_test


# In[45]:


from sklearn.linear_model import LinearRegression
lin_reg = LinearRegression()
lin_reg.fit(X, y)


# In[46]:


LinearRegression(copy_X=True, fit_intercept=True, n_jobs=None, normalize=False)


# In[47]:


def show_linear():
    plt.scatter(X, y, color ='blue')
    plt.plot(X, lin_reg.predict(X), color='green')
    plt.title('Linear Regression for Temperatures.')
    plt.xlabel('year')
    plt.ylabel('********************************')
    plt.show()
    return
show_linear()


# In[48]:


lin_reg.predict([[5.5]])


# In[49]:


from sklearn.preprocessing import PolynomialFeatures
poly_reg = PolynomialFeatures(degree=4)
X_poly = poly_reg.fit_transform(X)
pol_reg = LinearRegression()
pol_reg.fit(X_poly, y)


# In[50]:


def show_polymonial():
    plt.scatter(X, y, color ='red')
    plt.plot(X, pol_reg.predict(poly_reg.fit_transform(X)), color='blue')
    plt.title('Linear Regression for Temperatures.')
    plt.xlabel('year')
    plt.ylabel('********************************')
    plt.show()
    return
show_polymonial()


# In[51]:


pol_reg.predict(poly_reg.fit_transform([[5.5]]))


# In[52]:


predicted ={}
for i in range(1,31):
    yhat = -53.21 + 0.0371 * (2000 + i)
    predicted[2020 + i] = yhat


# In[53]:


predicted

