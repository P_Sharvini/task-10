#!/usr/bin/env python
# coding: utf-8

# In[21]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm


# In[22]:


data = pd.read_csv('Temperature.csv')


# In[23]:


data


# In[24]:


data.describe()


# In[25]:


y = data['year']
x1 = data['AverageTemperatureFahr']


# In[26]:


plt.scatter(x1,y)
yhat = 0.8*x1 + 10
fig = plt.plot(x1,yhat, lw=4, c='orange',label='regression line')
plt.xlabel('AverageTemperatureFahr',fontsize=10)
plt.ylabel('year',fontsize=10)
plt.show()

